package com.example.aa.pospay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.aa.pospay.Adapter.ImageAdapter;

public class pembayaranFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pembayaran_layout, container, false);

        GridView gridview = view.findViewById(R.id.gridView);
        gridview.setMinimumHeight(85);
        gridview.setAdapter(new ImageAdapter(getActivity()));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                // Launch ViewImage.java using intent
                Intent goPLN, goPDAM, goPulsa, goTiket, goPajak, goFinance, goTokoOnline, goTelkom, goAsuransi, goTvKabel, goPasca;

                goPLN = new Intent(getActivity(), pln.class);
                goPDAM = new Intent(getActivity(), pdam.class);
                goPulsa = new Intent(getActivity(), pulsa.class);
                goTiket = new Intent(getActivity(), tiket.class);
                goPasca = new Intent(getActivity(), pascaBayar.class);
                goPajak = new Intent(getActivity(), pajak.class);
                goFinance = new Intent(getActivity(), m_finance.class);
                goTelkom = new Intent(getActivity(), telkom.class);
                goTokoOnline = new Intent(getActivity(), tokoOnline.class);
                goAsuransi = new Intent(getActivity(), asuransi.class);
                goTvKabel = new Intent(getActivity(), tv_kabel.class);

                // Send captured position to ViewImage.java

                // Start ViewImage.java
                if (position == 0) {
                    startActivity(goPLN);
                } else if (position == 1) {
                    startActivity(goPDAM);
                } else if (position == 2) {
                    startActivity(goPulsa);
                } else if (position == 3) {
                    startActivity(goTiket);
                } else if (position == 4) {
                    startActivity(goPasca);
                } else if (position == 5) {
                    startActivity(goFinance);
                } else if (position == 6) {
                    startActivity(goPajak);
                } else if (position == 7) {
                    startActivity(goTelkom);
                } else if (position == 8) {
                    startActivity(goTokoOnline);
                } else if (position == 9) {
                    startActivity(goAsuransi);
                } else if (position == 10) {
                    startActivity(goTvKabel);
                } else {
                    Toast.makeText(getActivity(), "Maaf icon ini belum diproses layoutnya", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

}
