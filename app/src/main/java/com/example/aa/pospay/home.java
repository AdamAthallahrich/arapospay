package com.example.aa.pospay;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class home extends AppCompatActivity {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    FragmentManager FM;
    FragmentTransaction FT;

    FirebaseDatabase database;
    DatabaseReference users;

    TextView nama_lengkap;

    ImageView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.shitstuff);

        FM = getSupportFragmentManager();
        FT = FM.beginTransaction();
        FT.replace(R.id.containerView, new TabFragment()).commit();

        info = findViewById(R.id.BtnInfo);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(home.this, goInfo.class));
            }
        });

        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // Here, thisActivity is the current activity
        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // Permission is not granted
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "Membutuhkan Izin Lokasi", Toast.LENGTH_SHORT).show();
            } else {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);
            }
        } else {
            // Permission has already been granted
            Toast.makeText(this, "Izin Lokasi diberikan", Toast.LENGTH_SHORT).show();
        }
    }

    public void BtnLogout(View view) {
        Intent i = new Intent(home.this, MainActivity.class);
        startActivity(i);
    }

    public void goProfile(View view) {
        Intent i = new Intent(this, goProfile.class);
        startActivity(i);
    }

    public void goMutasi(View view) {
        Intent i = new Intent(this, goMutasi.class);
        startActivity(i);
    }

    public void goRekap(View view) {
        Intent i = new Intent(this, goRekap.class);
        startActivity(i);
    }

    public void goHubungiKami(View view) {
        Intent i = new Intent(this, goContactService.class);
        startActivity(i);
    }

    public void goSetting(View view) {
        Intent i = new Intent(this, goSetting.class);
        startActivity(i);
    }

    public void goTransaksi(View view) {
        Intent i = new Intent(this, goTransaksi.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        // Simply Do noting!
    }

}