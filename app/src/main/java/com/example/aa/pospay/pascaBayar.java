package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class pascaBayar extends AppCompatActivity {

    ArrayAdapter<String> kartu;
    String kartu_perdana[] = {"Telkomsel", "XL", "Indosat"};
    String kartuP = "";
    Spinner kp;

    TextView provider, nt;
    Button cek;
    LinearLayout ct;
    private TextInputEditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasca_bayar);

        kp = findViewById(R.id.perdana);
        kartu = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kartu_perdana);
        kp.setAdapter(kartu);
        kp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        kartuP = "Telkomsel";
                        break;
                    case 1:
                        kartuP = "XL";
                        break;
                    case 2:
                        kartuP = "Indosat";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        provider = findViewById(R.id.pilihKartu);
        nt = findViewById(R.id.nomorTelp);
        cek = findViewById(R.id.cek_data);
        input = findViewById(R.id.inputNomer);
        ct = findViewById(R.id.cetak);
        Toast.makeText(this, "Klik form untuk mengisi Nomer anda 2x", Toast.LENGTH_SHORT).show();

        input.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cek.setVisibility(View.VISIBLE);
                cek.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (String.valueOf(input.getText()).isEmpty()) {
                            Toast.makeText(getApplication(), "Isi Terlebih dahulu Nomer Telepon", Toast.LENGTH_SHORT).show();
                        } else {
                            nt.setText(input.getText());
                            provider.setText(kartuP);
                            Toast.makeText(getApplication(), "Cek terlebih dahulu", Toast.LENGTH_SHORT).show();
                            ct.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbarPascaBayar);
        mToolbar.setTitle("Pembayaran Jenis Pasca Bayar");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button bayar = findViewById(R.id.beli);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(pascaBayar.this, paymentPascaBayarSucces.class));
            }
        });

    }
}
