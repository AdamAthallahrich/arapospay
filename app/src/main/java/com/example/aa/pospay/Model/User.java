package com.example.aa.pospay.Model;

public class User {
    private String mId, mNamaLengkap, mPassword, mMacAddress;

    public User() {
    }

    public User(String id, String nama_lengkap, String password, String mac_address) {
        if (nama_lengkap.trim().equals("")) {
            nama_lengkap = "";
        }

        mId = id;
        mNamaLengkap = nama_lengkap;
        mPassword = password;
        mMacAddress = mac_address;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getNama() {
        return mNamaLengkap;
    }

    public void setNama(String nama_lengkap) {
        mNamaLengkap = nama_lengkap;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getMac() {
        return mMacAddress;
    }

    public void setMac(String macAddress) {
        mMacAddress = macAddress;
    }

}
