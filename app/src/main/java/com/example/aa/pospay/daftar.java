package com.example.aa.pospay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aa.pospay.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class daftar extends AppCompatActivity {

    Button generateId, BtnDaftar;
    TextView IdUser, macAddress;
    EditText namaUser, password;

    FirebaseDatabase database;
    DatabaseReference users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        macAddress = findViewById(R.id.mac);
        macAddress.setText(generateID());

        namaUser = findViewById(R.id.nama);
        password = findViewById(R.id.password);
        BtnDaftar = findViewById(R.id.BtnDaftar);

        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        generateId = findViewById(R.id.generate);
        IdUser = findViewById(R.id.id_user);
        generateId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IdUser.setText(generateString(5));
            }
        });

        BtnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final User user = new User(IdUser.getText().toString(), namaUser.getText().toString(), password.getText().toString(), macAddress.getText().toString());

                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child(user.getNama()).exists() && dataSnapshot.child(user.getPassword()).exists()) {
                            Toast.makeText(daftar.this, "Nama dan Password sudah ada", Toast.LENGTH_SHORT).show();
                        } else if (dataSnapshot.child(user.getId()).exists()) {
                            Toast.makeText(daftar.this, "Id sudah ada", Toast.LENGTH_SHORT).show();
                        } else if (dataSnapshot.child(user.getNama()).exists()) {
                            Toast.makeText(daftar.this, "Nama sudah ada", Toast.LENGTH_SHORT).show();
                        } else {
                            users.child(user.getId()).setValue(user);
                            Toast.makeText(daftar.this, "Sukses Mendaftarkan Akun", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });

    }

    private String generateString(int length) {
        char[] chars = "QWERTYUIOPASDFGHJKLZXCVBNMmnbvcxzlkjhgfdsapoiuytrewq1234567890".toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    private String generateID() {
        String deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        if ("9774d56d682e549c".equals(deviceId) || deviceId == null) {
            deviceId = ((TelephonyManager) this
                    .getSystemService(Context.TELEPHONY_SERVICE))
                    .getDeviceId();
            if (deviceId == null) {
                Random tmpRand = new Random();
                deviceId = String.valueOf(tmpRand.nextLong());
            }
        }
        return getHash(deviceId);
    }

    public String getHash(String stringToHash) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        byte[] result = null;
        try {
            result = digest.digest(stringToHash.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();

        for (byte b : result) {
            sb.append(String.format("%02X", b));
        }

        String messageDigest = sb.toString();
        return messageDigest;
    }

    public void pdhLogin(View view) {
        Intent intent = new Intent(daftar.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // Simply Do noting!
    }

}
