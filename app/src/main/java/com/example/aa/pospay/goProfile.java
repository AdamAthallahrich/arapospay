package com.example.aa.pospay;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class goProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_profile);

        Toolbar mToolbar = findViewById(R.id.toolbarProfile);
        mToolbar.setTitle("Akun Profile");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        TextInputEditText UserID = findViewById(R.id.userID);
        TextInputEditText Usename = findViewById(R.id.username);
        TextInputEditText Password = findViewById(R.id.pass);
        TextInputEditText saldo = findViewById(R.id.saldo);
        TextInputEditText komisi = findViewById(R.id.komisi);
        TextInputEditText total_transaksi = findViewById(R.id.total_transaksi);
        UserID.setEnabled(false);
        Usename.setEnabled(false);
        Password.setEnabled(false);
        saldo.setEnabled(false);
        komisi.setEnabled(false);
        total_transaksi.setEnabled(false);

    }
}
