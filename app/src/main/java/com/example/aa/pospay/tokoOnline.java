package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class tokoOnline extends AppCompatActivity {

    Spinner TokoOnline;
    ArrayAdapter<String> jenisTokoOnline;
    String produk[] = {"Blibli", "Tokopedia", "Lazada", "Elevenia"};
    String produk1 = "";

    TextInputEditText noPelanggan;
    TextView cetakNoPelanggan, cetakJenisTvKabel;
    Button buttonCetakTagihan, bayar;
    LinearLayout cetakLinearTagihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toko_online);

        TokoOnline = findViewById(R.id.jenisTokoOnline);
        jenisTokoOnline = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, produk);
        TokoOnline.setAdapter(jenisTokoOnline);
        TokoOnline.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        produk1 = "Blibli";
                        break;
                    case 1:
                        produk1 = "Tokopedia";
                        break;
                    case 2:
                        produk1 = "Lazada";
                        break;
                    case 3:
                        produk1 = "Elevenia";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar9);
        mToolbar.setTitle("Top-Up Toko Online");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button bayar = findViewById(R.id.BtnBayarTokoOnline);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusTokoOnline.class));
            }
        });

        noPelanggan = findViewById(R.id.nomerID);
        cetakNoPelanggan = findViewById(R.id.cetakID);
        cetakJenisTvKabel = findViewById(R.id.cetakTokoOnline);
        buttonCetakTagihan = findViewById(R.id.BtnTagihan);
        cetakLinearTagihan = findViewById(R.id.cetakLinear1);

        buttonCetakTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(noPelanggan.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Nomer Pelanggan terlebih dahulu", Toast.LENGTH_SHORT).show();
                } else {
                    cetakNoPelanggan.setText(String.valueOf(noPelanggan.getText()));
                    cetakJenisTvKabel.setText(produk1);
                    cetakLinearTagihan.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
