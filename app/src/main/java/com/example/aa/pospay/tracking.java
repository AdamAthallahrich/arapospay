package com.example.aa.pospay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aa.pospay.Adapter.TrackingAdapter;
import com.example.aa.pospay.Model.DaftarTracking;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class tracking extends Fragment {

    DatabaseReference database;
    private List<DaftarTracking> trackingList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TrackingAdapter mAdapter;

    public tracking() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.tracking, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);

        mAdapter = new TrackingAdapter(getActivity().getApplicationContext(), trackingList);
        //set layout manager to recycler view
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //set Adapter to recycler view
        recyclerView.setAdapter(mAdapter);
        FirebaseCrash.log("App Started");
        prepareTrackingData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        prepareTrackingData();
    }

    private void prepareTrackingData() {
        trackingList.clear();
        database = FirebaseDatabase.getInstance().getReference("Tracking");
        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                trackingList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    DaftarTracking m = postSnapshot.getValue(DaftarTracking.class);
                    DaftarTracking tracking = new DaftarTracking(m.getId(), m.getName(), m.getStatus(), m.getDestinasiAwal(), m.getTujuan(), m.getJenis(), m.getAddresss(), m.getHarga(), postSnapshot.getKey());
                    trackingList.add(tracking);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
