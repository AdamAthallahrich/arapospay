package com.example.aa.pospay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aa.pospay.Adapter.ImageAdapter;

public class SingleItemView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_item_view);

        Intent i = getIntent();

        int position = i.getExtras().getInt("id");

        // Open the Image adapter
        ImageAdapter imageAdapter = new ImageAdapter(this);

        // Locate the ImageView in single_item_view.xml
        ImageView imageView = findViewById(R.id.imageItem);
        TextView textView = findViewById(R.id.textItem);

        // Get image and position from ImageAdapter.java and set into ImageView
        textView.setText(imageAdapter.mText[position]);
        imageView.setImageResource(imageAdapter.mThumbIds[position]);

    }
}
