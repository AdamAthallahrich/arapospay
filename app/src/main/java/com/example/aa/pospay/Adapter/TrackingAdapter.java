package com.example.aa.pospay.Adapter;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.aa.pospay.Model.DaftarTracking;
import com.example.aa.pospay.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

public class TrackingAdapter extends RecyclerView.Adapter<TrackingAdapter.MyViewHolder> {

    public Context mContext;
    DatabaseReference database;
    private List<DaftarTracking> trackingList;

    public TrackingAdapter(Context mContext, List<DaftarTracking> trackingList) {
        this.trackingList = trackingList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final DaftarTracking tracking = trackingList.get(position);
        holder.idUser.setText(tracking.getId());
        holder.namaUser.setText(tracking.getName());
        holder.status.setText(tracking.getStatus());
        holder.destinasiAwal.setText(tracking.getDestinasiAwal());
        holder.tujuanDestinasi.setText(tracking.getTujuan());
        holder.jenisBarang.setText(tracking.getJenis());
        holder.alamatTujuan.setText(tracking.getAddresss());
        holder.hargaKirim.setText(tracking.getHarga());
        holder.b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //point databaseReference to Movies
                database = FirebaseDatabase.getInstance().getReference("Tracking");
                //remove value of child movie.getKey()
                database.child(tracking.getKey()).setValue(null);
                //remove item from list
                trackingList.remove(position);
                //notofy datachanged to adapter
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, trackingList.size());
                Toast.makeText(mContext, "Pengiriman Dibatalkan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return trackingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextInputEditText idUser, namaUser, status, destinasiAwal, tujuanDestinasi, jenisBarang, alamatTujuan, hargaKirim;
        Button b3;

        public MyViewHolder(View view) {
            super(view);
            b3 = view.findViewById(R.id.BtnDelete);
            idUser = view.findViewById(R.id.IdUser);
            namaUser = view.findViewById(R.id.NamaUser);
            status = view.findViewById(R.id.StatusBarangUser);
            destinasiAwal = view.findViewById(R.id.DestinasiAwal);
            tujuanDestinasi = view.findViewById(R.id.Tujuan);
            jenisBarang = view.findViewById(R.id.JenisBarangUser);
            alamatTujuan = view.findViewById(R.id.AlamatTujuan);
            hargaKirim = view.findViewById(R.id.HargaCetak);

            idUser.setEnabled(false);
            namaUser.setEnabled(false);
            status.setEnabled(false);
            destinasiAwal.setEnabled(false);
            tujuanDestinasi.setEnabled(false);
            jenisBarang.setEnabled(false);
            alamatTujuan.setEnabled(false);
            hargaKirim.setEnabled(false);
        }

    }
}
