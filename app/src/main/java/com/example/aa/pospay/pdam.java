package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class pdam extends AppCompatActivity {

    Spinner pdam, spWilayah;
    ArrayAdapter<String> kotaAsalPDAM, kotaTengah, kotaBarat, wilayah;
    String kotaAsal[] = {"(4237)\tPDAM KOTA KEDIRI", "(4238)\tPDAM KAB.PROBOLINGGO", "(4240)\tPDAM KAB.NGANJUK"};
    String kotaPdam = "";
    String kotaJateng[] = {"(4023)\tPDAM KOTA SEMARANG", "(4026)\tPDAM Kab. Semarang"};
    String kotaJabar[] = {"(4027)\tPDAM KOTA BANDUNG", "(4028)\tPDAM KAB. BANDUNG"};
    String macamWilayah[] = {"Jawa Timur", "Jawa Barat", "Jawa Tengah"};
    String setWilayah = "";

    Button cariIdPdam, bayar;
    TextInputEditText IdPelanggan;
    TextView ShowIdPdam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdam);

        pdam = findViewById(R.id.pdam);
        spWilayah = findViewById(R.id.wilayah);
        final TextView wilayahPdam = findViewById(R.id.WilayahPdam);
        wilayah = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, macamWilayah);
        spWilayah.setAdapter(wilayah);
        kotaAsalPDAM = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kotaAsal);
        kotaTengah = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kotaJateng);
        kotaBarat = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kotaJabar);
        spWilayah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        setWilayah = "Jawa Timur";
                        break;
                    case 1:
                        setWilayah = "Jawa Barat";
                        break;
                    case 2:
                        setWilayah = "Jawa Tengah";
                        break;
                }
                wilayahPdam.setText(setWilayah);
                if (wilayahPdam.getText() == "Jawa Timur") {
                    pdam.setAdapter(kotaAsalPDAM);
                    pdam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            switch (position) {
                                case 0:
                                    kotaPdam = "(4237)\tPDAM KOTA KEDIRI";
                                    break;
                                case 1:
                                    kotaPdam = "(4238)\tPDAM KAB.PROBOLINGGO";
                                    break;
                                case 2:
                                    kotaPdam = "(4240)\tPDAM KAB.NGANJUK";
                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else if (wilayahPdam.getText() == "Jawa Tengah") {
                    pdam.setAdapter(kotaTengah);
                    pdam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            switch (position) {
                                case 0:
                                    kotaPdam = "(4023)\tPDAM KOTA SEMARANG";
                                    break;
                                case 1:
                                    kotaPdam = "(4026)\tPDAM Kab. Semarang";
                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                } else if (wilayahPdam.getText() == "Jawa Barat") {
                    pdam.setAdapter(kotaBarat);
                    pdam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            switch (position) {
                                case 0:
                                    kotaPdam = "(4027)\tPDAM KOTA BANDUNG";
                                    break;
                                case 1:
                                    kotaPdam = "(4028)\tPDAM KAB. BANDUNG";
                                    break;
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar1);
        mToolbar.setTitle("Pembayaran PDAM");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        cariIdPdam = findViewById(R.id.BtnCariPDAM);
        IdPelanggan = findViewById(R.id.id_pdam);
        ShowIdPdam = findViewById(R.id.IDPdam);
        final LinearLayout cetak = findViewById(R.id.cetak);
        final TextView perusahaanPdam = findViewById(R.id.perusahaanPDAM);
        cariIdPdam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = String.valueOf(IdPelanggan.getText());
                if (id.isEmpty()) {
                    Toast.makeText(pdam.this, "Isilah Id Pelanggan Terlebih dahulu", Toast.LENGTH_SHORT).show();
                } else {
                    ShowIdPdam.setText(id);
                    perusahaanPdam.setText(kotaPdam);
                    cetak.setVisibility(View.VISIBLE);
                }
            }
        });
        bayar = findViewById(R.id.beli);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusPdam.class));
            }
        });
    }
}
