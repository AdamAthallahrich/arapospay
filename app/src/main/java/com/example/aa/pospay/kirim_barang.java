package com.example.aa.pospay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aa.pospay.Model.DaftarTracking;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.app.Activity.RESULT_OK;

public class kirim_barang extends Fragment {

    public static final int kirimBarang = 0;
    public static final int destinasiBarang = 1;
    public static final int alamatDestinasi = 2;
    private static int REQUEST_CODE = 0;
    Spinner sj;
    ArrayAdapter<String> jenisBarang;
    String isiSpinner[] = {"Surat", "Paket"};
    String jenisKiriman = "";
    DatabaseReference database;
    Button SubmitButton;
    String IdUser = "0", Status = "None";
    EditText namaPengirim;
    private TextView tvKirimDari, tvDestinasiLokasi, tvAlamatDestinasi, HargaOngkos;

    public kirim_barang() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.kirim_barang, container, false);

        tvKirimDari = view.findViewById(R.id.pengirimanAwal);
        tvDestinasiLokasi = view.findViewById(R.id.tujuanPengiriman);
        tvAlamatDestinasi = view.findViewById(R.id.alamatDestinasi);
        sj = view.findViewById(R.id.spinnerJenis);
        jenisBarang = new ArrayAdapter<String>(kirim_barang.this.getActivity(), android.R.layout.simple_list_item_1, isiSpinner);
        namaPengirim = view.findViewById(R.id.namaPengirim);

        HargaOngkos = view.findViewById(R.id.harga);
        SubmitButton = view.findViewById(R.id.submit);
        database = FirebaseDatabase.getInstance().getReference("Tracking");

        SubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DaftarTracking m = new DaftarTracking(IdUser, namaPengirim.getText().toString(), Status, tvKirimDari.getText().toString(), tvDestinasiLokasi.getText().toString(), jenisKiriman, tvAlamatDestinasi.getText().toString(), HargaOngkos.getText().toString());
                database.push().setValue(m);
                IdUser = "";
                namaPengirim.setText("");
                Status = "";
                tvKirimDari.setText("Destinasi Awal");
                tvDestinasiLokasi.setText("Destinasi Pengiriman");
                jenisKiriman = null;
                tvAlamatDestinasi.setText("Alamat Destinasi");
                HargaOngkos.setText("10.000");
                Toast.makeText(getActivity(), "Barang Anda Akan Segera Diproses\nSilahkan Kirim Barang Anda ke Kantor Pos Terdekat", Toast.LENGTH_SHORT).show();
            }
        });

        sj.setAdapter(jenisBarang);
        sj.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        jenisKiriman = "Surat";
                        break;
                    case 1:
                        jenisKiriman = "Paket";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvKirimDari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoComplete(kirimBarang);
            }
        });
        tvDestinasiLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoComplete(destinasiBarang);
            }
        });
        tvAlamatDestinasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AutoComplete(alamatDestinasi);
            }
        });

        return view;
    }

    private void AutoComplete(int tipeLokasi) {
        REQUEST_CODE = tipeLokasi;
        // Filter hanya tmpat yg ada di Indonesia
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("ID").build();
        try {
            // Intent untuk mengirim Implisit Intent
            Intent mIntent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(typeFilter)
                    .build(getActivity());
            // jalankan intent impilist
            startActivityForResult(mIntent, REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace(); // cetak error
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace(); // cetak error
            // Display Toast
            Toast.makeText(getActivity(), "Layanan Play Services Tidak Tersedia", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Toast.makeText(this, "Sini Gaes", Toast.LENGTH_SHORT).show();
        // Pastikan Resultnya OK
        if (resultCode == RESULT_OK) {
            //Toast.makeText(this, "Sini Gaes2", Toast.LENGTH_SHORT).show();
            // Tampung Data tempat ke variable
            Place placeData = PlaceAutocomplete.getPlace(getActivity(), data);

            if (placeData.isDataValid()) {
                // Show in Log Cat
                Log.d("autoCompletePlace Data", placeData.toString());
                // Dapatkan Detail
                String placeAddress = placeData.getAddress().toString();
                String placeName = placeData.getName().toString();
                // Cek user milih titik jemput atau titik tujuan
                switch (REQUEST_CODE) {
                    case kirimBarang:
                        // Set ke widget lokasi asal
                        tvKirimDari.setText(placeName);
                        break;
                    case destinasiBarang:
                        // Set ke widget lokasi tujuan
                        tvDestinasiLokasi.setText(placeName);
                        break;
                    case alamatDestinasi:
                        tvAlamatDestinasi.setText(placeAddress);
                        break;
                }
            } else {
                // Data tempat tidak valid
                Toast.makeText(getActivity(), "Kota Yang Anda Cari tidak ditemukan !", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
