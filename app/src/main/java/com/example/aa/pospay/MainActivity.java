package com.example.aa.pospay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.aa.pospay.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    EditText IdUser, passwd;
    Button BtnLogin;

    FirebaseDatabase database;
    DatabaseReference users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IdUser = findViewById(R.id.id_user);
        passwd = findViewById(R.id.password);
        BtnLogin = findViewById(R.id.BtnLogin);

        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");


        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });

    }

    private void signIn() {
        final String id_login = IdUser.getText().toString();
        final String password_user = passwd.getText().toString();
        users.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(id_login).exists()) {
                    if (!id_login.isEmpty()) {
                        User login = dataSnapshot.child(id_login).getValue(User.class);
                        if (login.getPassword().equals(password_user)) {
                            Toast.makeText(getApplicationContext(), "Sukses Login", Toast.LENGTH_SHORT).show();
                            Intent in = new Intent(MainActivity.this, home.class);
                            startActivity(in);
                            ProgressBar loadi = findViewById(R.id.loading);
                            loadi.setVisibility(View.VISIBLE);
                            BtnLogin.setVisibility(View.INVISIBLE);
                            LinearLayout textBawah = findViewById(R.id.textBawah);
                            textBawah.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(getApplicationContext(), "Password Salah", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Id belum diisi", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void pdhDaftar(View view) {
        Intent intent1 = new Intent(MainActivity.this, daftar.class);
        startActivity(intent1);
    }

    @Override
    public void onBackPressed() {
        // Simply Do noting!
    }

}
