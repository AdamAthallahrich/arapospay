package com.example.aa.pospay;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class goInfo extends AppCompatActivity {

    TextView depo, depo1, visi, visi1;
    LinearLayout showDepo, showVisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_info);

        Toolbar mToolbar = findViewById(R.id.toolbarInfo);
        mToolbar.setTitle("Info");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        depo = findViewById(R.id.selectDeposit);
        depo1 = findViewById(R.id.selectDeposit1);
        showDepo = findViewById(R.id.showDeposit);
        depo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDepo.setVisibility(View.VISIBLE);
                depo.setVisibility(View.GONE);
                depo1.setVisibility(View.VISIBLE);
            }
        });
        depo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDepo.setVisibility(View.GONE);
                depo.setVisibility(View.VISIBLE);
                depo1.setVisibility(View.GONE);
            }
        });

        visi = findViewById(R.id.selectVisi);
        visi1 = findViewById(R.id.selectVisi1);
        showVisi = findViewById(R.id.showVisi);
        visi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVisi.setVisibility(View.VISIBLE);
                visi.setVisibility(View.GONE);
                visi1.setVisibility(View.VISIBLE);
            }
        });
        visi1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVisi.setVisibility(View.GONE);
                visi.setVisibility(View.VISIBLE);
                visi1.setVisibility(View.GONE);
            }
        });
    }
}
