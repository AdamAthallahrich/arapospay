package com.example.aa.pospay.Model;

public class DaftarTracking {

    private String Id, Name, Status, DestinasiAwal, Tujuan, Jenis, Addresss, Harga, Key;

    public DaftarTracking() {
    }

    public DaftarTracking(String id, String name, String status, String destinasiAwal, String tujuan, String jenis, String addresss, String harga, String key) {
        this.Id = id;
        this.Name = name;
        this.Status = status;
        this.DestinasiAwal = destinasiAwal;
        this.Tujuan = tujuan;
        this.Jenis = jenis;
        this.Addresss = addresss;
        this.Harga = harga;
        this.Key = key;
    }

    public DaftarTracking(String id, String name, String status, String destinasiAwal, String tujuan, String jenis, String addresss, String harga) {
        this.Id = id;
        this.Name = name;
        this.Status = status;
        this.DestinasiAwal = destinasiAwal;
        this.Tujuan = tujuan;
        this.Jenis = jenis;
        this.Addresss = addresss;
        this.Harga = harga;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getDestinasiAwal() {
        return DestinasiAwal;
    }

    public void setDestinasiAwal(String destinasiAwal) {
        this.DestinasiAwal = destinasiAwal;
    }

    public String getTujuan() {
        return Tujuan;
    }

    public void setTujuan(String tujuan) {
        this.Tujuan = tujuan;
    }

    public String getJenis() {
        return Jenis;
    }

    public void setJenis(String jenis) {
        this.Jenis = jenis;
    }

    public String getAddresss() {
        return Addresss;
    }

    public void setAddresss(String alamat) {
        this.Addresss = alamat;
    }

    public String getHarga() {
        return Harga;
    }

    public void setHarga(String harga) {
        this.Harga = harga;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        this.Key = key;
    }
}
