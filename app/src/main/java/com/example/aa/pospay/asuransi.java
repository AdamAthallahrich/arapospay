package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class asuransi extends AppCompatActivity {

    Spinner jA;
    ArrayAdapter<String> jenisAsuransi;
    String produk[] = {"BPJS Kesehatan", "Premi Jiwasraya", "Prudential"};
    String produk1 = "";
    TextInputEditText jmlBulan, noVirtual;
    TextView cetakVirtual, jumlahBulan, cetakJenisProduk;
    Button tagihan, bayar;
    LinearLayout cetakLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asuransi);

        bayar = findViewById(R.id.BtnBayarAsuransi);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusAsuransi.class));
            }
        });

        jA = findViewById(R.id.jenisAsuransi);
        jenisAsuransi = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, produk);
        jA.setAdapter(jenisAsuransi);
        jA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        produk1 = "BPJS Kesehatan";
                        break;
                    case 1:
                        produk1 = "Premi Jiwasraya";
                        break;
                    case 2:
                        produk1 = "Prudential";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar7);
        mToolbar.setTitle("Pembayaran Asuransi");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        jmlBulan = findViewById(R.id.jumlahBulan);
        noVirtual = findViewById(R.id.nomerVirtual);
        cetakVirtual = findViewById(R.id.cetakNomerVirtual);
        jumlahBulan = findViewById(R.id.cetakJumlahBulan);
        cetakJenisProduk = findViewById(R.id.cetakJenisAsuransi);
        tagihan = findViewById(R.id.BtnTagihan);
        cetakLinear = findViewById(R.id.lineCetak);

        tagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(noVirtual.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Nomer Virtual terlebih dahulu", Toast.LENGTH_SHORT).show();
                } else {
                    cetakVirtual.setText(String.valueOf(noVirtual.getText()));
                    jumlahBulan.setText(String.valueOf(jmlBulan.getText()));
                    cetakJenisProduk.setText(produk1);
                    cetakLinear.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
