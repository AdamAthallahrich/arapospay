package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class tv_kabel extends AppCompatActivity {

    Spinner jTVKabel;
    ArrayAdapter<String> jenisTvKabel;
    String produk[] = {"Indovision", "AORA TV", "ORANGE TV", "K-VISION TV Kabel"};
    String produk1 = "";

    TextInputEditText noPelanggan;
    TextView cetakNoPelanggan, cetakJenisTvKabel;
    Button buttonCetakTagihan, bayar;
    LinearLayout cetakLinearTagihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_kabel);

        jTVKabel = findViewById(R.id.jenisTVKabel);
        jenisTvKabel = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, produk);
        jTVKabel.setAdapter(jenisTvKabel);
        jTVKabel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        produk1 = "Indovision";
                        break;
                    case 1:
                        produk1 = "AORA TV";
                        break;
                    case 2:
                        produk1 = "ORANGE TV";
                        break;
                    case 3:
                        produk1 = "K-VISION TV Kabel";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar8);
        mToolbar.setTitle("Pembayaran TV Kabel");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bayar = findViewById(R.id.BtnBayarTvKabel);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusTvKabel.class));
            }
        });

        noPelanggan = findViewById(R.id.nomerPelanggan);
        cetakNoPelanggan = findViewById(R.id.cetakNomerPelanggan);
        cetakJenisTvKabel = findViewById(R.id.cetakJenisTvKabel);
        buttonCetakTagihan = findViewById(R.id.BtnTagihan);
        cetakLinearTagihan = findViewById(R.id.cetakLinear);

        buttonCetakTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(noPelanggan.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Nomer Pelanggan terlebih dahulu", Toast.LENGTH_SHORT).show();
                } else {
                    cetakNoPelanggan.setText(String.valueOf(noPelanggan.getText()));
                    cetakJenisTvKabel.setText(produk1);
                    cetakLinearTagihan.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
