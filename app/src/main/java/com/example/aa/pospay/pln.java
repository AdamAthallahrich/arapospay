package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class pln extends AppCompatActivity {

    Spinner sp, nm;
    EditText display_id;
    TextView id_cetak;

    LinearLayout cetak;

    String jumlah_nominal[] = {"20.000", "50.000", "100.000"};

    ArrayAdapter<String> adapter, nominal;
    String record = "", nominalKwh = "", harga = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pln);

        nm = findViewById(R.id.jumlah_nominal);

        nominal = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, jumlah_nominal);

        display_id = findViewById(R.id.id_pelanggan);

        cetak = findViewById(R.id.cetak);

        id_cetak = findViewById(R.id.id_pelangganCetak);

        nm.setAdapter(nominal);
        nm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        nominalKwh = "20Kwh";
                        harga = "Rp. 22.000";
                        break;
                    case 1:
                        nominalKwh = "50Kwh";
                        harga = "Rp. 52.000";
                        break;
                    case 2:
                        nominalKwh = "100Kwh";
                        harga = "Rp. 102.000";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar2);
        mToolbar.setTitle("Pembayaran PLN");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final Button cetakTagihan = findViewById(R.id.BtnCetak);
        final Button cetakTagihanPostPaid = findViewById(R.id.cetakTagihanPosPaid);
        final Button cetakTagihanNonTaglis = findViewById(R.id.BtnCetakTagihanNonTaglis);

        final Button prepaid, postpaid, nontaglis, Bayar;
        prepaid = findViewById(R.id.prePaid);
        postpaid = findViewById(R.id.postPaid);
        nontaglis = findViewById(R.id.nonTagLis);
        Bayar = findViewById(R.id.bayar);
        final LinearLayout prePaid = findViewById(R.id.pilihPrePaid);

        final EditText idPelangganPostPaid = findViewById(R.id.id_pelangganPostPaid);
        final EditText nomerRegistrasi = findViewById(R.id.id_pelangganTagLis);

        final TextView cetakIdPelangganPostPaid = findViewById(R.id.cetakIDPelangganPostPaid);
        final TextView cetakNomerRegistrasi = findViewById(R.id.cetakNomerRegistrasi);

        final LinearLayout postPaid = findViewById(R.id.pilihPostPaid);
        final LinearLayout cetakPostPaid = findViewById(R.id.cetakPostPaid);
        final LinearLayout nonTagList = findViewById(R.id.pilihNonTagLis);
        final LinearLayout cetakNonTagList = findViewById(R.id.cetakNonTagList);

        nontaglis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(nomerRegistrasi.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Terlebih dahulu Nomer Registrasi", Toast.LENGTH_LONG).show();
                } else {
                    postPaid.setVisibility(View.GONE);
                    cetakTagihan.setVisibility(View.GONE);
                    prePaid.setVisibility(View.GONE);
                    cetak.setVisibility(View.GONE);
                    cetakTagihanPostPaid.setVisibility(View.GONE);
                    cetakPostPaid.setVisibility(View.GONE);
                    nonTagList.setVisibility(View.VISIBLE);
                    cetakTagihanNonTaglis.setVisibility(View.VISIBLE);
                    cetakTagihanNonTaglis.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cetakNonTagList.setVisibility(View.VISIBLE);
                            cetakNomerRegistrasi.setText(nomerRegistrasi.getText());
                            Toast.makeText(getApplicationContext(), "Silahkan cek kembali, Sudah Benar atau Belum", Toast.LENGTH_LONG).show();
                            nomerRegistrasi.setText("");
                        }
                    });
                    Bayar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getApplicationContext(), cekStatusPLNNonTagList.class));
                            Toast.makeText(getApplicationContext(), "Cek Status NonTagList", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(idPelangganPostPaid.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Terlebih dahulu Id Pelanggan", Toast.LENGTH_LONG).show();
                } else {
                    postPaid.setVisibility(View.VISIBLE);
                    cetakTagihan.setVisibility(View.GONE);
                    prePaid.setVisibility(View.GONE);
                    cetak.setVisibility(View.GONE);
                    cetakTagihanPostPaid.setVisibility(View.VISIBLE);
                    nonTagList.setVisibility(View.GONE);
                    cetakTagihanNonTaglis.setVisibility(View.GONE);
                    cetakNonTagList.setVisibility(View.GONE);
                    cetakTagihanPostPaid.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cetakIdPelangganPostPaid.setText(idPelangganPostPaid.getText());
                            idPelangganPostPaid.setText("");
                            cetakPostPaid.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(), "Silahkan cek kembali, Sudah Benar atau Belum", Toast.LENGTH_LONG).show();
                        }
                    });
                    Bayar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getApplicationContext(), cekStatusPLNPostPaid.class));
                            Toast.makeText(getApplicationContext(), "Cek Status PostPaid", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        prepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(display_id.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Terlebih dahulu Id Pelanggan/Nomer Meter", Toast.LENGTH_LONG).show();
                } else {
                    prePaid.setVisibility(View.VISIBLE);
                    cetakTagihan.setVisibility(View.VISIBLE);
                    postPaid.setVisibility(View.GONE);
                    cetakPostPaid.setVisibility(View.GONE);
                    cetakTagihanPostPaid.setVisibility(View.GONE);
                    nonTagList.setVisibility(View.GONE);
                    cetakTagihanNonTaglis.setVisibility(View.GONE);
                    cetakNonTagList.setVisibility(View.GONE);
                    cetakTagihan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cetak.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(), "Silahkan cek kembali, Sudah Benar atau Belum", Toast.LENGTH_LONG).show();

                            String id = display_id.getText().toString();
                            String nomorMeter = "";
                            TextView nomorMeterCetak = findViewById(R.id.nomerMeterCetak);
                            nomorMeterCetak.setText(nomorMeter);
                            id_cetak.setText(id);

                            display_id.setText("");
                        }
                    });
                    Bayar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(getApplicationContext(), cekStatusPLNPrePaid.class));
                            Toast.makeText(getApplicationContext(), "Cek Status PrePaid", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }
}
