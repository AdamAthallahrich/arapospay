package com.example.aa.pospay;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import static com.example.aa.pospay.TabFragment.int_items;

public class MyAdapter extends FragmentPagerAdapter {

    public MyAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new pembayaranFragment();
            case 1:
                return new kirim_barang();
            case 2:
                return new tracking();
        }
        return null;
    }

    @Override
    public int getCount() {
        return int_items;
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Pembayaran";
            case 1:
                return "Kirim Barang";
            case 2:
                return "Tracking";
        }
        return null;
    }

}
