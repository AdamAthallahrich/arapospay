package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class pulsa extends AppCompatActivity {

    Spinner kp, nmp, nmd;
    Button pp, pd, pra, pasca, cek_data;
    LinearLayout PP, PD, cetakan;
    EditText nomer;
    TextView pilihKartu, pilihan, pulsa_data, nominalKartu, nyetak_nomer, hargaBayar;
    LinearLayout cetak;

    ArrayAdapter<String> nomData, nomPulsa, kartu;

    String text = "Nominal               : ";

    String kartu_perdana[] = {"Telkomsel", "XL", "Indosat"};
    String nominal_pulsa[] = {"20.000", "50.000", "100.000"};
    String paketan_data[] = {"1Gb", "2Gb", "5Gb", "7Gb", "10Gb", "15Gb", "20Gb"};

    String kartuP = "", nominalP = "", nominalD = "", harga_data = "", harga_pulsa = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulsa);

        kp = findViewById(R.id.perdana);
        nmp = findViewById(R.id.nominal_pulsa);
        nmd = findViewById(R.id.nominal_data);

        nomer = findViewById(R.id.nomer_telp);
        nyetak_nomer = findViewById(R.id.nomorTelp);

        pp = findViewById(R.id.pilihPulsa);
        pd = findViewById(R.id.pilihPaket);
        cek_data = findViewById(R.id.cek_data);
        final Button bayar = findViewById(R.id.beli);

        PP = findViewById(R.id.pulsa);
        PD = findViewById(R.id.paket_data);
        cetak = findViewById(R.id.cetak);

        pilihan = findViewById(R.id.txtPilihlah);
        pulsa_data = findViewById(R.id.pilihPaketAtauPulsa);
        pilihKartu = findViewById(R.id.pilihKartu);
        nominalKartu = findViewById(R.id.pilihNominal);
        hargaBayar = findViewById(R.id.harga);

        kartu = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kartu_perdana);
        nomPulsa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nominal_pulsa);
        nomData = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, paketan_data);

        nmd.setAdapter(nomData);
        nmd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        nominalD = "1Gb";
                        harga_data = "Rp. 15.000";
                        break;
                    case 1:
                        nominalD = "2Gb";
                        harga_data = "Rp. 25.000";
                        break;
                    case 2:
                        nominalD = "5Gb";
                        harga_data = "Rp. 60.000";
                        break;
                    case 3:
                        nominalD = "7Gb";
                        harga_data = "Rp. 75.000";
                        break;
                    case 4:
                        nominalD = "10Gb";
                        harga_data = "Rp. 110.000";
                        break;
                    case 5:
                        nominalD = "15Gb";
                        harga_data = "Rp. 155.000";
                        break;
                    case 6:
                        nominalD = "20Gb";
                        harga_data = "Rp. 210.000";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        nmp.setAdapter(nomPulsa);
        nmp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        nominalP = "20.000";
                        harga_pulsa = "22.000";
                        break;
                    case 1:
                        nominalP = "Rp. 50.000";
                        harga_pulsa = "52.000";
                        break;
                    case 2:
                        nominalP = "Rp. 100.000";
                        harga_pulsa = "102.000";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                nominalP = "Pilih Nominal Anda";
            }
        });

        kp.setAdapter(kartu);
        kp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        kartuP = "Telkomsel";
                        break;
                    case 1:
                        kartuP = "XL";
                        break;
                    case 2:
                        kartuP = "Indosat";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cek_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pulsa_data.getText() == "") {
                    Toast.makeText(getApplicationContext(), "Pilihlah Pulsa atau Paket Data", Toast.LENGTH_SHORT).show();
                } else {
                    cetak.setVisibility(View.VISIBLE);
                    nominalKartu.setText("");
                    pilihKartu.setText("Kartu Perdana    : " + kartuP);
                    String noP = String.valueOf(nominalKartu.getText());
                    if (noP.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Apakah Pilihan Anda Sudah Benar", Toast.LENGTH_SHORT).show();
                    }
                    nominalKartu.setText(text + nominalP + nominalD);
                    String i = String.valueOf(nomer.getText());
                    nyetak_nomer.setText("Nomor Telepon  : " + i);
                    if (i.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Isikan Nomer Telp Anda Terlebih Dahulu", Toast.LENGTH_SHORT).show();
                    }
                    hargaBayar.setText("Harga     : " + harga_data + harga_pulsa);
                }

            }
        });

        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cetak.setVisibility(View.GONE);
                PD.setVisibility(View.INVISIBLE);
                PP.setVisibility(View.VISIBLE);
                pilihan.setVisibility(View.INVISIBLE);
                pulsa_data.setText("Pulsa");
                nominalKartu.setText(text);
                harga_data = "";
                nominalD = "";
                bayar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), cekStatusPulsa.class));
                    }
                });
            }
        });
        pd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cetak.setVisibility(View.GONE);
                PD.setVisibility(View.VISIBLE);
                PP.setVisibility(View.INVISIBLE);
                pilihan.setVisibility(View.INVISIBLE);
                pulsa_data.setText("Paket Data");
                nominalKartu.setText(text);
                harga_pulsa = "";
                nominalP = "";
                bayar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), cekStatusPaketan.class));
                    }
                });
            }
        });

        Toolbar mToolbar = findViewById(R.id.toolbar3);
        mToolbar.setTitle("Pembayaran Pulsa dan Paketan");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
