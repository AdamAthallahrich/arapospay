package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class m_finance extends AppCompatActivity {

    Spinner jenisProduk;
    ArrayAdapter<String> jenisFinance;
    String produk[] = {"FIF Finance", "OTO Finance", "WOM Finance"};
    String produk1 = "";

    Button cari, Bayar;
    LinearLayout linear, khusus;
    TextInputEditText inputID;
    TextView cetakID, cetakProduk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_m_finance);

        Toolbar mToolbar = findViewById(R.id.toolbar5);
        mToolbar.setTitle("Pembayaran Multi Finance");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        khusus = findViewById(R.id.khusus);

        jenisProduk = findViewById(R.id.produkFinance);
        jenisFinance = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, produk);
        jenisProduk.setAdapter(jenisFinance);
        jenisProduk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        produk1 = "FIF Finance";
                        khusus.setVisibility(View.GONE);
                        break;
                    case 1:
                        produk1 = "OTO Finance";
                        khusus.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        produk1 = "WOM Finance";
                        khusus.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cari = findViewById(R.id.BtnCariID);
        Bayar = findViewById(R.id.BtnBayarFinance);
        linear = findViewById(R.id.line2);
        inputID = findViewById(R.id.IDPelanggan);
        cetakID = findViewById(R.id.CetakIDPelanggan);
        cetakProduk = findViewById(R.id.cetakProduk);

        Bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusMFinance.class));
            }
        });

        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(inputID.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isikan Terlebih dahulu Id Pelanggan", Toast.LENGTH_SHORT).show();
                } else {
                    linear.setVisibility(View.VISIBLE);
                    String cetak = String.valueOf(inputID.getText());
                    cetakID.setText(cetak);
                    cetakProduk.setText(produk1);
                }
            }
        });
    }
}
