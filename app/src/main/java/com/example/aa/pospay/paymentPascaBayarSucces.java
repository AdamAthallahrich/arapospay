package com.example.aa.pospay;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

public class paymentPascaBayarSucces extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_pulsa);

        Toolbar mToolbar = findViewById(R.id.toolbarPaymentPulsaSukses);
        mToolbar.setTitle("Pembelian Pasca Bayar Sukses");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Toast.makeText(this, "Transaksi Anda Telah Berhasil\nSilahkan cek Pulsa atau Paketan Anda", Toast.LENGTH_SHORT).show();
    }
}
