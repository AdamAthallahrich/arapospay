package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class telkom extends AppCompatActivity {

    Button cetakTagihan, bayar;
    TextInputEditText idTelkom;
    TextView cetakId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telkom);

        Toolbar mToolbar = findViewById(R.id.toolbar6);
        mToolbar.setTitle("Pembayaran Telkom");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Button bayar = findViewById(R.id.BtnBayarTelkom);
        bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusTelkom.class));
            }
        });

        cetakTagihan = findViewById(R.id.BtnTagihan);
        idTelkom = findViewById(R.id.IDPelangganTelkom);
        cetakId = findViewById(R.id.cetakPelanggan);
        final LinearLayout linearCetak = findViewById(R.id.cetakTagihan);

        cetakTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(idTelkom.getText()).isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Isi Id Pelanggan Telkom terlebih dahulu", Toast.LENGTH_SHORT).show();
                } else {
                    cetakId.setText(String.valueOf(idTelkom.getText()));
                    linearCetak.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
