package com.example.aa.pospay.Firebase;

import com.example.aa.pospay.Model.DaftarTracking;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class FirebaseHelper {

    DatabaseReference db;
    Boolean saved;
    ArrayList<DaftarTracking> trackings = new ArrayList<>();

    public FirebaseHelper(DatabaseReference db) {
        this.db = db;
    }

    public Boolean save(DaftarTracking tracking) {

        if (tracking == null) {
            saved = false;
        } else {
            try {
                db.child("Tracking").push().setValue(tracking);
                saved = true;
            } catch (DatabaseException e) {
                e.printStackTrace();
                saved = false;
            }
        }
        return saved;
    }

    private void fetchData(DataSnapshot dataSnapshot) {
        trackings.clear();
        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            DaftarTracking t = new DaftarTracking();
            t.setId(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setName(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setStatus(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setJenis(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setDestinasiAwal(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setTujuan(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setAddresss(String.valueOf(ds.getValue(DaftarTracking.class)));
            t.setHarga(String.valueOf(ds.getValue(DaftarTracking.class)));
            trackings.add(t);
        }
    }

    public ArrayList<DaftarTracking> retrieve() {
        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return trackings;
    }

}
