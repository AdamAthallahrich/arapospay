package com.example.aa.pospay;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;

public class goMutasi extends AppCompatActivity {

    private static final String TAG = "goMutasi";
    TextInputEditText noStruk, debet, kredit, transaksi, hargaTransaksi, keterangan;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private TextInputEditText mDisplayDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_mutasi);

        noStruk = findViewById(R.id.noStruk);
        debet = findViewById(R.id.debet);
        kredit = findViewById(R.id.kredit);
        transaksi = findViewById(R.id.transaksi);
        hargaTransaksi = findViewById(R.id.hargaTransaksi);
        keterangan = findViewById(R.id.keterangan);

        noStruk.setEnabled(false);
        debet.setEnabled(false);
        kredit.setEnabled(false);
        transaksi.setEnabled(false);
        hargaTransaksi.setEnabled(false);
        keterangan.setEnabled(false);

        mDisplayDate = findViewById(R.id.inputTanggal);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(goMutasi.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = day + "-" + month + "-" + year;
                mDisplayDate.setText(date);
            }
        };

        Toolbar mToolbar = findViewById(R.id.toolbarMutasi);
        mToolbar.setTitle("Laporan Koran");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
