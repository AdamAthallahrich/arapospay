package com.example.aa.pospay;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class pajak extends AppCompatActivity {

    TextInputEditText NPWP, Tahun;
    TextView noP, tahunTagihan;
    TextView kota;
    Button DataTagihan, BayarTagihan;

    Spinner pajak, spWilayah;
    ArrayAdapter<String> kotaAsalPajak, kotaTengah, kotaBarat, wilayah;
    String kotaAsal[] = {"(8008)\tPBB KAB. BANYUWANGI", "(8028)\tPKB JAWA TIMUR", "(8072)\tPBB KAB.SIDOARJO"};
    String kotaPajak = "";
    String kotaJateng[] = {"(8084)\tPBB PROVINSI JAWA TENGAH"};
    String kotaJabar[] = {"(8076)\tPBB KOTA TANGERANG", "(8074)\tPBB KOTA BANDUNG"};
    String macamWilayah[] = {"Jawa Timur", "Jawa Barat", "Jawa Tengah"};
    String setWilayah = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pajak);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Pembayaran Pajak");
        mToolbar.setTitleTextColor(Color.WHITE);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        kota = findViewById(R.id.kota);

        pajak = findViewById(R.id.pajak);
        spWilayah = findViewById(R.id.wilayah);
        final TextView wilayahPdam = findViewById(R.id.WilayahPdam);
        wilayah = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, macamWilayah);
        spWilayah.setAdapter(wilayah);
        kotaAsalPajak = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kotaAsal);
        kotaTengah = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kotaJateng);
        kotaBarat = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, kotaJabar);
        spWilayah.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        setWilayah = "Jawa Timur";
                        pajak.setAdapter(kotaAsalPajak);
                        pajak.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                switch (position) {
                                    case 0:
                                        kotaPajak = "(8008)\tPBB KAB. BANYUWANGI";
                                        break;
                                    case 1:
                                        kotaPajak = "(8028)\tPKB JAWA TIMUR";
                                        break;
                                    case 2:
                                        kotaPajak = "(8072)\tPBB KAB.SIDOARJO";
                                        break;
                                }
                                kota.setText(kotaPajak);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        break;
                    case 1:
                        setWilayah = "Jawa Barat";
                        pajak.setAdapter(kotaBarat);
                        pajak.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                switch (position) {
                                    case 0:
                                        kotaPajak = "(8076)\tPBB KOTA TANGERANG";
                                        break;
                                    case 1:
                                        kotaPajak = "(8074)\tPBB KOTA BANDUNG";
                                        break;
                                }
                                kota.setText(kotaPajak);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        break;
                    case 2:
                        setWilayah = "Jawa Tengah";
                        pajak.setAdapter(kotaTengah);
                        pajak.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                switch (position) {
                                    case 0:
                                        kotaPajak = "(8084)\tPBB PROVINSI JAWA TENGAH";
                                        break;
                                }
                                kota.setText(kotaPajak);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        NPWP = findViewById(R.id.NPWP);
        Tahun = findViewById(R.id.tahun);
        noP = findViewById(R.id.noP);
        tahunTagihan = findViewById(R.id.tahunTagihan);
        DataTagihan = findViewById(R.id.BtnDataTagihan);
        BayarTagihan = findViewById(R.id.BtnBayarTagihan);
        BayarTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), cekStatusPajak.class));
            }
        });
        final LinearLayout transparan = findViewById(R.id.line2);

        DataTagihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String npwp = String.valueOf(NPWP.getText());
                String tahun = String.valueOf(Tahun.getText());
                if (npwp == "") {
                    Toast.makeText(getApplicationContext(), "Isi NOP Anda", Toast.LENGTH_SHORT).show();
                } else if (tahun == "") {
                    Toast.makeText(getApplicationContext(), "Isi Tahun yang akan di cek", Toast.LENGTH_SHORT).show();
                } else if (npwp == "" && tahun == "") {
                    Toast.makeText(getApplicationContext(), "Isi NOP dan Tahun yang akan di cek", Toast.LENGTH_SHORT).show();
                } else {
                    noP.setText(npwp);
                    tahunTagihan.setText(tahun);
                    transparan.setVisibility(View.VISIBLE);
                }
            }
        });

    }
}
