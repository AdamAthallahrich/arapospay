package com.example.aa.pospay.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aa.pospay.R;

public class ImageAdapter extends BaseAdapter {
    // References to our images in res > drawable
    public String[] mText = {"PLN", "PDAM", "Pulsa & Paketan", "Tiket\nKereta & Pesawat", "Pasca Bayar", "M-Finance", "PBB", "Pembayaran\nTelkom", "Toko Online", "Asuransi", "TV Kabel"};
    public Integer[] mThumbIds = {R.drawable.ic_logo_pln, R.drawable.ic_logo_pdam, R.drawable.ic_logo_pulsa_paket, R.drawable.ic_logo_tiket, R.drawable.ic_logo_pasca_bayar, R.drawable.ic_logo_finance, R.drawable.ic_logo_pajak, R.drawable.ic_logo_pembayaran_telkom, R.drawable.ic_logo_toko_online, R.drawable.ic_logo_asuransi, R.drawable.ic_logo_tv_kabel};
    private Context mContext;

    public ImageAdapter(Context c) {
        mContext = c;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // Create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        // if it's not recycled, initialize some attributes
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.activity_single_item_view, null);
            TextView tv = v.findViewById(R.id.textItem);
            tv.setText(mText[position]);
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            ImageView iv = v.findViewById(R.id.imageItem);
            iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
            iv.setMaxWidth(85);
            iv.setMaxWidth(85);
//iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            iv.setPadding(8, 8, 8, 8);
            iv.setImageResource(mThumbIds[position]);
        } else {
            v = convertView;
        }
        return v;
    }
}